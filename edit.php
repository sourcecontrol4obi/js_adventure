<?php
$_GET['id'] = $_GET['v'] ;
$_GET['type'] = "cta" ;
$_GET['ap'] = 1 ;

$video_id = $_GET['id'];
?>



<?php
	
require_once '../../create/getter-riot.php';
require_once '../../create/simple-html-dom.php';

// $wrapper = '<div class="editor" data-editable data-name="main-content">';


$additional_headers = 
	'<link class="app-script" rel="stylesheet" type="text/css" href="node_modules/font-awesome/css/font-awesome.min.css"/>'.
	'<link class="app-script" rel="stylesheet" type="text/css" href="bower_components/jquery-ui/themes/dot-luv/jquery-ui.min.css"/>'.
	'<link class="app-script" rel="stylesheet" type="text/css" href="libs/spectrum/spectrum.css"/>'.
	'<link class="app-script" rel="stylesheet" type="text/css" href="css/style.css"/>'.
	'<link class="app-script" rel="stylesheet" type="text/css" href="css/actions.css"/>';
    
$additional_footer = 
   '<div class="app-controls">
        <div id="controls" class="control"></div>
    
        <div id="elementSelector" class="control"></div>

        <div id="saveBtn" class="control" onclick="modes['."'save'".'](event)">
            <i class="fa fa-save"></i>
        </div>

        <div id="editModeBtn" class="control" onclick="modes['."'enterEditMode'".'](event)">
            <i class="fa fa-edit"></i>
        </div>
    </div>   
    <script>window.post_url = "/3d/page/edit.php?v='.$video_id.'";</script>
   	<script class="app-script" type="text/javascript" src="node_modules/jquery/dist/jquery.js"></script>
	<script class="app-script" type="text/javascript" src="bower_components/jquery-ui/jquery-ui.min.js"></script>
	<script class="app-script" type="text/javascript" src="node_modules/rx/dist/rx.all.js"></script>
	<script class="app-script" type="text/javascript" src="node_modules/rx-jquery/rx.jquery.js"></script>
	<script class="app-script" type="text/javascript" src="libs/spectrum/spectrum.js"></script>
	<script class="app-script" type="text/javascript" src="libs/plugin.js"></script>   	
   	<script class="app-script" type="text/javascript" src="libs/handlebars-1.0.0.js"></script>
    <script class="app-script" type="text/javascript" src="js/mouseInputs.js"></script>
    <script class="app-script" type="text/javascript" src="js/main.js"></script>   	
    ';
	
$url = ( $datax['source']->webm->url ) ;

$html = new simple_html_dom();

$html->load_file($url);

$head = $html->find('head', -1);
$body = $html->find('body', -1);

if($head){
	$head->innertext = $additional_headers.$head->innertext;
}
if($body){
	$body->innertext = '<div class="editor">'.$body.innertext.'</div>'.$additional_footer;
}

echo $html->save();
?>