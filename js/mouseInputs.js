(function(){
    var MouseInputs = window.MouseInputs = function(element){
        var excludeChildrenOf = '.app-controls';
        this.click = $(element)
                        .delegateAsObservable('*', 'click') 
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .filter(function(el){                        
                            return el.target.tagName.toUpperCase() !== 'HTML';
                        });

        this.dblclick = $(element)
                        .delegateAsObservable('*', 'click') 
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .filter(function(ev){     
                            ev.preventDefault();
                            return ev.target.tagName.toUpperCase() !== 'HTML';
                        });

        this.down = $(element)
                        .mousedownAsObservable();

        this.drag = $(element)
                        .onAsObservable('drag')
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .map(function(ev){
                            return ev.originalEvent;
                        });

        this.dragstart = $(element)
                        .delegateAsObservable('*', 'dragstart')
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .map(function(ev){
                            ev.originalEvent.dataTransfer.dropEffect = "move";
                            return ev.originalEvent;
                        });

        this.dragover = $(element)
                        .delegateAsObservable('*', 'dragover')
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .map(function(ev){
                            ev.originalEvent.preventDefault();
                            ev.originalEvent.dataTransfer.dropEffect = "move";
                            return ev.originalEvent;
                        });

        this.drop = $(element)
                        .delegateAsObservable('*', 'drop')
                        .filter(function(e){
                            return e.target === e.currentTarget && !$.contains($(excludeChildrenOf)[0], e.target);
                        })
                        .map(function(ev){  
                            ev.originalEvent.preventDefault();
                            return ev.originalEvent;
                        });
    }
})();