(function(){          
    if(location.pathname.indexOf('js_adventure') > 0){
        $.handlebars({
            templatePath: 'tpl',
            templateExtension: 'hbs'
        }); 
    }else if(location.origin.indexOf('localhost') > 0){
        $.handlebars({
            templatePath: 'tpl',
            templateExtension: 'hbs'
        }); 
    }else{
        $.handlebars({
            templatePath: 'js_adventure/tpl',
            templateExtension: 'hbs'
        });
    }          

    Handlebars.registerHelper({
        hasAttr: function(prop, options) {                
            if(prop in this){
                return options.fn(this);
            }
        },
        equals: function(context, options){                    
            var parts = context.split('==');                    
            if(this[parts[0]] === parts[1]){                        
                return options.fn(this);
            }
        },
        trim: function(context, options){
            return context.trim();
        }
    });

    // state properties
    var mouseInputs = new MouseInputs('.editor');
    var activeElement = window.activeElement = null;
    var currentDropEvent = window.currentDropEvent = null;
    var currentCursorPos = window.currentCursorPos = null;
    var htmlElements = window.htmlElements = null;
    var configuredElement = window.configuredElement = null;
    var controls = $('#controls');
    var elementSelector = $('#elementSelector');
    // end state properties

    // actions holder
    var actions = window.actions = {
        move:function(){
            startDrag(activeElement);
            controls.empty();
        },
        delete:function(){
            $(activeElement).remove();
            controls.empty();
        },
        edit:function(){
            var editable = $(activeElement).attr('contenteditable');
            if(editable === undefined){
                $(activeElement).attr('contenteditable', true);
            }else{
                $(activeElement).attr('contenteditable', editable === 'true' ? false : true);
            }
            controls.empty();
        },
        copy:function(){
            var clone = $(activeElement).clone(true);
            clone.removeAttr('id');
            clone.removeClass('active');
            clone.insertAfter(activeElement);
            controls.empty();
        },
        close:function(){
            $(activeElement).removeClass('active');
            controls.empty();
            activeElement = null;
        },
        addElement:function(type, index){                    
            // create new element                                        
            var tagName = htmlElements[type][index]['tagName'];
            configuredElement = document.createElement(tagName);
            if(tagName.toUpperCase() === 'IFRAME'){
                $(configuredElement).addClass('candrag');
            }
            // show element property setter
            elementSelector.render('elementProperties', configuredElement)
            .dialog({
                title:'Configure ' + tagName,
                close:function(ev, ui){
                    elementSelector.find('[name$="Color"], [name$="color"]').spectrum("destroy");                            
                }
            })
            .find('.accordion').accordion({heightStyle:'content'});
            setTimeout(function(){
                elementSelector.find('.accordion').accordion({heightStyle:'content'});
                elementSelector.find('[name$="Color"], [name$="color"]').spectrum({
                    showInput: true,
                    allowEmpty:true,
                    preferredFormat:'hex3'
                });
            }, 500);                                                          
        },
        onConfigureElement:function(ev, tag){
            var data = $(ev.target).serializeArray();
            data = data.reduce(function(prev, curr, i){
                if(i === 1){
                    var d = {};
                    d[prev.name] = prev.value;
                    d[curr.name] = curr.value;
                    return d;
                }else{
                    prev[curr.name] = curr.value;
                    return prev;
                }
            });                                                            

            var el = configuredElement;
            for(var key in data){
                el[key] = data[key];
                if(key === 'textContent'){
                    el[key] = data[key].trim();
                }else{
                    el.style[key] = data[key];
                }
            }

            // show drop point select
            if(configuredElement !== activeElement){
                controls.render('dropPositionSelector', {key: '', type: 'create'});     
                var pos = getPosition(activeElement, {width:280, height: 45});                    
                controls.css({left:pos.left, top:pos.top, position: 'fixed'});                        
            }                    
            elementSelector.dialog("close");
            return false;
        },
        setDropPosition:function(type, pos, key){
            var target, dragged;                    
            switch(type){
                case 'create':
                    target = activeElement;
                    dragged = $(configuredElement);  
                    break;
                case 'event':
                    target = currentDropEvent.target;
                    dragged = $(key);  
                    break;
            }                                                                         
            switch(pos){
                case 'append':
                    $(target).append(dragged);
                    break;
                case 'prepend':
                    $(target).prepend(dragged);
                    break;
                case 'before':
                    $(dragged).insertBefore(target);
                    break;
                case 'after':
                    $(dragged).insertAfter(target);
                    break;
                case 'cursor':                            
                    if(currentCursorPos){                                                                
                        currentCursorPos.insertNode(dragged[0]);
                    }else{
                        alert('no cursor position selected');
                    }
                    break;
            }
            dragged.removeAttr('drag-key');
            dragged.removeAttr('draggable');
            controls.empty();
        }
    }    

    var modes = window.modes = {
        editMode : false,
        children : ["saveBtn"],
        save:function(ev){
            var html = $('html').clone();
            html.find('.app-controls').remove();
            html.find('.app-script').remove();
            console.log(html.html());
        },
        configureElement:function(ev, tag){
            configuredElement = activeElement;
            var modalConfig = {
                width:'70vw', modal:true, title:'Configure ' + configuredElement.tagName, 
                dialogClass: "app-controls",
                close:function(ev, ui){
                    elementSelector.find('[name$="Color"], [name$="color"]').spectrum("destroy");                            
                }
            };                    
            elementSelector.render('elementProperties', configuredElement)
            .dialog(modalConfig)
            .find('.accordion').accordion({heightStyle:'content'});
            setTimeout(function(){
                elementSelector.find('.accordion').accordion({heightStyle:'content'});
                elementSelector.find('[name$="Color"], [name$="color"]').spectrum({
                    showInput: true,
                    allowEmpty:true,
                    preferredFormat:'hex3'
                });
            }, 500);
        },
        addElement : function(e){                    
            loadElementSelector();
        },
        enterEditMode : function(e){
            var el = $(e.target);
            if(el.attr('edit-mode') === 'true'){
                modes.children.forEach(function(child, i){
                    $('#' + child).animate({
                        right : (i + 1) * el.width() + 20
                    })
                });
                el.removeAttr('edit-mode');
                $('.active').removeClass('active');
                $('iframe').removeClass('candrag');
                $('[draggable]').removeAttr('draggable');
                try {
                    elementSelector.dialog('destroy');
                } catch (error) {
                    
                }
                elementSelector.empty();
                controls.empty();
                activeElement = null;
                modes.editMode = false;
            }else{
                modes.children.forEach(function(child, i){
                    $('#' + child).animate({
                        right : el.css('right')
                    })
                });                        
                el.attr('edit-mode', true);
                $('iframe').addClass('candrag');
                modes.editMode = true;
            }
        }
    }
    // end actions holder

    var loadElementSelector = function(){
        var modalConfig = {width:'70vw', modal:true, title:'Select an element', dialogClass: "app-controls"};
        if(htmlElements !== null){
            elementSelector.render('elementSelector', htmlElements);                    
            elementSelector.dialog(modalConfig)
            .find('.content').accordion({heightStyle:'content'});
            // setTimeout(function(){
            //     elementSelector.find('.content').accordion({heightStyle:'content'});
            // }, 500);
            return;
        }
        $.getJSON('res/elements.json')
            .done(function(data){     
                htmlElements = data;                                           
                elementSelector.render('elementSelector', data);                        
                elementSelector.dialog(modalConfig)
                .find('.content').accordion({heightStyle:'content'});
                setTimeout(function(){
                    elementSelector.find('.content').accordion({heightStyle:'content'});
                }, 500);
            });
    }

    var startDrag = function(element){
        var el = $(activeElement);
        var draggable = el.attr('draggable');
        if(draggable){
            el.removeAttr('draggable');
        }else{                
            el.attr('draggable', true);
        }
    }
    
    var getPosition = function(ele1, dim){                
        var offset = $(ele1).offset();
        var w1 = $(ele1).width();
        var w2 = dim.width;
        var h1 = $(ele1).height();
        var h2 = dim.height;
        var left = offset.left + (w1 - w2)/2;
        var top = offset.top + (h1 - h2)/2;
        left = left > 0 ? left : 0;
        return {
            top: $(window).height()/2 - h2,
            left: $(window).width()/2 - w2/2
        }
    }

    // necessary for drag operations
    mouseInputs.drag
    .filter(function(ev){
        return $(ev.target).attr('draggable') === 'true';
    })
    .subscribe()

    // necessary for drag operations
    mouseInputs.dragstart
    .filter(function(ev){
        return $(ev.target).attr('draggable') === 'true';
    })           
    .subscribe(function(ev){                
        var key = new Date().getTime();
        $(ev.target).attr('drag-key', key);                
        ev.dataTransfer.setData("key", key);
    });

    // necessary for drag operations
    mouseInputs.dragover
    .filter(function(ev){
        return ev.dataTransfer.getData('key')? true : false;
    })
    .subscribe();

    // drop operation subscription
    mouseInputs.drop  
    .filter(function(ev){
        return ev.dataTransfer.getData('key')? true : false;
    })          
    .subscribe(function(ev){
        currentDropEvent = ev;
        var key = ev.dataTransfer.getData('key');
        var dragged = "[drag-key="+ key +"]";     
        
        // show drop point select
        controls.render('dropPositionSelector', {key: dragged, type: 'event'});     
        var pos = getPosition(ev.target, {width:310, height: 45});                
        $('#controls').css({left:pos.left, top:pos.top, position: 'fixed'});
    });

    // set the active element            
    mouseInputs.click.subscribe(function(event){
        if(!modes.editMode){                    
            return;
        }
        if($(event.target).attr('contenteditable') === 'true' && !event.altKey){
            if(event.ctrlKey){                        
                var el = $(event.target);                    
                if(el.attr('contenteditable') === 'true'){
                    el.removeAttr('contenteditable');
                }
            }
            else{
                return;
            }
        }
        if(event.target === event.currentTarget){ 
            // pass to edit button handler                                          
            if($(event.target).attr('draggable') == 'true'){                       
                return;
            }       
            if(event.altKey){                        
                var sel = window.getSelection();                        
                if(sel.rangeCount > 0){                            
                    currentCursorPos = sel.getRangeAt(0);
                    
                } 
            }
            event.preventDefault();    
            if(activeElement && !event.altKey){
                $(activeElement).removeClass('active');
                $(activeElement).removeAttr('contenteditable');
            }                    
            activeElement = event.target;                    
            $(activeElement).addClass('active');                    
            controls.render('actions', {tag : activeElement.tagName});     
            var pos = getPosition(activeElement, {width:172, height: 45});                    
            controls.css({left:pos.left, top:pos.top, position: 'fixed'});               
        }               
    });        
})(jQuery);